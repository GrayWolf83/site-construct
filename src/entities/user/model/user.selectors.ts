import { AppDispatch, RootState } from "@/app/providers/store-provider/store.types";
import { userActions } from "./user.slice";
import { RefreshRes } from "../api/user.api.types";
export const getUserAuthError = (state: RootState) => state.user.error;
export const getLoginError = (state: RootState) => state.user.error;
export const getUserIsAuth = (state: RootState): boolean => Boolean(state.user.user.accessToken);
export const getUserId = (state: RootState): string => state.user.user.userId;
export const getUser = (state: RootState) => state.user.user;

export const userLogout = () => (dispatch: AppDispatch) => {
  dispatch(userActions.logout());
};

export const userRefresh =
  (payload: RefreshRes & { expiresAccess: number; expiresRefresh: number }) =>
  (dispatch: AppDispatch) => {
    dispatch(userActions.refreshTokens(payload));
  };
