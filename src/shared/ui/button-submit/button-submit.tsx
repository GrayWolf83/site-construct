import { FC } from "react";
import cls from "./button-submit.module.scss";

interface ButtonSubmitProps {
  onSubmit: () => void;
  title?: string;
}

export const ButtonSubmit: FC<ButtonSubmitProps> = ({ onSubmit, title = "Submit" }) => {
  return (
    <button className={cls.button_form} onClick={onSubmit}>
      {title}
    </button>
  );
};
